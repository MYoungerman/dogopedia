import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Random from './views/Random.vue'
import Category from './views/Category.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
    path: '/about/:url',
    name: 'about',
    component: About,
    props: true
    },
    {
      path: '/random',
      name: 'random',
      component: Random
    },
    {
      path: '/category/:category',
      name: 'category',
      component: Category,
      props: true
    }
  ],
  scrollBehavior (to, from, savedPosition) {//Make the page scroll to top for all route navigations
  return { x: 0, y: 0 }
}
})


